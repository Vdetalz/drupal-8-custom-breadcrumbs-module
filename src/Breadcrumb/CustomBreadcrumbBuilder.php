<?php

namespace Drupal\custom_breadcrumb\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;

class CustomBreadCrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $attributes) {
    $parameters = $attributes->getParameters()->all();
    // I need my breadcrumbs for a few node types ONLY,
    // so it should be applied on node page ONLY.
    if (isset($parameters['node']) && !empty($parameters['node'])) {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute('Home', '<front>'));

    $node      = \Drupal::routeMatch()->getParameter('node');
    $node_type = $node->bundle();

    switch ($node_type) {
      // If node type is "bands"
      // It's adding as parent of breadcrumb summary bands view.
      case 'bands':
        $breadcrumb->addLink(Link::createFromRoute('Bands', 'view.bands.page_1'));

        break;

      // If node type is "article".
      // It's adding as parent of breadcrumb summary articles view.
      case 'article':
        $breadcrumb->addLink(Link::createFromRoute('Articles', 'view.articles.page_1'));

        break;
    }

    // Don't forget to add cache control by a route,
    // otherwise you will surprise,
    // all breadcrumb will be the same for all pages.
    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;
  }

}
